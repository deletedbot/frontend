print("Welcome to DeletedBot setup script!\nPlease answer the following questions to complete the setup.")
	
host = input("Enter the host of the backend. Ex: \"api.deletedbot.eu.org\": ")

if(input("Do you want to redirect to an external site for the privacy policy? [y/n] ") == "n"):
	if(input("Have you modified the privacy.txt file to suit your needs? [y/n] ") == "n"):
		print("Please do so and launch the script again.")
		exit()
	privacy = open("settings/privacy.txt", "r").read()
	use_file_for_privacy = True
else:
	privacy = input("Insert the URL where your users will be redirected to when reading the privacy policy: ")
	use_file_for_privacy = False
	
if(input("Do you want to redirect to an external site for the terms of service? [y/n] ") == "n"):
	if(input("Have you modified the terms.txt file to suit your needs? [y/n] ") == "n"):
		print("Please do so and launch the script again.")
		exit()
	terms = open("settings/terms.txt", "r").read()
	use_file_for_terms = True
else:
	terms = input("Insert the URL where your users will be redirected to when reading the terms of service: ")
	use_file_for_terms = False
	
if(input("Do you want to apply these settings? This is irreversible. [y/n] ") == "n"):
	exit()
	

f = open("res/create.js", "r+")
read = f.read().replace("[HOST]", host)
f.seek(0)
f.write(read)
f.truncate()
f.close()
f = open("index.html", "r+")
read = f.read().replace("[CONFIG]", "done")
f.seek(0)
f.write(read)
f.truncate()
f.close()

f = open("privacy.html", "r+")
if(use_file_for_privacy):
	privacy_out = f.read().replace("[BODY]", privacy)
else:
	privacy_out = f.read().replace("[REDIR]", "1").replace("[REDIRTO]", privacy).replace("[BODY]", "Redirecting... Please wait.")
f.seek(0)
f.write(privacy_out)
f.truncate()
f.close()

f = open("terms.html", "r+")
if(use_file_for_terms):
	terms_out = f.read().replace("[BODY]", terms)
else:
	terms_out = f.read().replace("[REDIR]", "1").replace("[REDIRTO]", terms).replace("[BODY]", "Redirecting... Please wait.")
f.seek(0)
f.write(terms_out)
f.truncate()
f.close()
